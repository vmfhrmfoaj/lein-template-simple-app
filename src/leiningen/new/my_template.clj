(ns leiningen.new.my-template
  "My Leiningen project templates.

  Usage:
   lein new my-template <PROJECT-NAME> [FLAGS] [OPTIONS]

   FLAGS:
    +config: add code/files related to the configuration

   OPTIONS:
    -type: <TYPE>

     TYPE:
      * api-server: API server
      * cli: CLI application
      * web-browser-ext: Web browser extension
      * web-server: Web server

   OPTIONS(api-server, web-server):
    -db: 'sqlite', 'postgresql'. (default: 'sqlite')"
  (:require [clojure.java.shell :refer [sh]]
            [clojure.string :as str]
            [comb.template :as tmpl]
            [leiningen.new.templates :as lein]
            [clojure.core :as core]))


(def ^:dynamic *data* nil)

(def render (lein/renderer "my-template" tmpl/eval))

(def raw (lein/raw-resourcer "my-template"))


(defn import-template
  "Print template file.

  Example:
   (import-template 12 \"\"])"
  ([indent tmpl-file]
   (import-template indent tmpl-file true))
  ([indent tmpl-file trim?]
   (let [indent-str (apply str (repeat indent " "))
         tmpl (render (str "snippets/" tmpl-file) *data*)
         out (some->> tmpl
                      (str/split-lines)
                      (map #(when-not (str/blank? %)
                              (str indent-str %)))
                      (interpose "\n")
                      (apply str))]
     (print (cond-> out trim? (str/trim))))))


(defn- append-conf-files
  "Append configuration files

  Example:
   (append-conf-files [...] {...})"
  [files data]
  (concat files [["spec/{{sanitized}}/core_spec.clj"     (render "core_spec.clj" data)]
                 ["spec/{{sanitized}}/config_spec.clj"   (render "config_spec.clj" data)]
                 ["src/clojure/spec/alpha/extension.clj" (render "spec-ext.clj" data)]
                 ["src/{{sanitized}}/config/core.clj"    (render "config-core.clj" data)]
                 ["src/{{sanitized}}/config/error.clj"   (render "config-error.clj" data)]]))


(defn- api-server-data
  "Returns data for 'api-server' template.

  Example:
   (api-server-data {:db \"sqlite\"})"
  [opt]
  (let [default {:db "sqlite"}]
    (merge default
           opt
           {:template :api-server
            :flag #{:db-schema :server}
            :platform #{:java}})))


(defn- api-server-files
  "Returns paths for 'api-server' template.

  Example:
   (api-server-files {:name \"...\", ...})"
  [data]
  (cond-> [[".env"                                          (render "_env" data)]
           ["Makefile"                                      (render "Makefile.api" data)]
           ["project.clj"                                   (render "project.api.clj" data)]
           ["tests.edn"                                     (render "tests.edn" data)]
           ,"data/migrations/"
           ["gen/{{sanitized}}/core_gen.clj"                (render "core_gen.clj" data)]
           ,"resources/static/"
           ["spec/{{sanitized}}/core_spec.clj"              (render "core_spec.clj" data)]
           ["src/{{sanitized}}/app/core.clj"                (render "app-core.api.clj" data)]
           ["src/{{sanitized}}/app/echo/core.clj"           (render "app-echo-core.clj" data)]
           ["src/{{sanitized}}/core.clj"                    (render "core.server.clj" data)]
           ["src/{{sanitized}}/db/core.clj"                 (render "db-core.clj" data)]
           ["src/{{sanitized}}/db/var.clj"                  (render "db-var.clj" data)]
           ["src/{{sanitized}}/internal/db_util.clj"        (render "internal-db_util.clj" data)]
           ["src/{{sanitized}}/web_server/core.clj"         (render "web-core.clj" data)]
           ["src/{{sanitized}}/web_server/server.clj"       (render "web-server.clj" data)]
           ["src/{{sanitized}}/web_server/var.clj"          (render "web-var.clj" data)]
           ["test/{{sanitized}}/app/core_test.clj"          (render "app-core_test.api.clj" data)]
           ["test/{{sanitized}}/core_test.clj"              (render "core_test.clj" data)]
           ["test/{{sanitized}}/internal/db_util_test.clj"  (render "internal-db_util_test.clj" data)]
           ["test/{{sanitized}}/test_util.clj"              (render "test_util.clj" data)]
           ["test/spy/extension.clj"                        (render "spy-ext.clj" data)]
           ["tool/dev/tool.clj"                             (render "tool-dev-tool.clj" data)]]
    (data :config) (append-conf-files data)))


(defn- cli-data
  "Returns data for 'cli' template.

  Example:
   (cli-data [...])"
  [opt]
  (let [default nil]
    (merge default
           opt
           {:template :cli
            :flag #{:cli}
            :platform #{:java}})))


(defn- cli-files
  "Returns paths for 'cli' template.

  Example:
   (cli-files {:name \"...\", ...})"
  [data]
  (cond-> [["Makefile"                          (render "Makefile.cli" data)]
           ["project.clj"                       (render "project.cli.clj" data)]
           ["tests.edn"                         (render "tests.edn" data)]
           ["gen/{{sanitized}}/core_gen.clj"    (render "core_gen.clj" data)]
           ["spec/{{sanitized}}/core_spec.clj"  (render "core_spec.clj" data)]
           ["src/{{sanitized}}/core.clj"        (render "core.clj" data)]
           ["test/{{sanitized}}/core_test.clj"  (render "core_test.clj" data)]
           ["test/{{sanitized}}/test_util.clj"  (render "test_util.clj" data)]
           ["test/spy/extension.clj"            (render "spy-ext.clj" data)]
           ["tool/dev/tool.clj"                 (render "tool-dev-tool.clj" data)]]
    (data :config) (append-conf-files data)))


(defn- web-browser-ext-data
  "Returns data for 'web-browser-ext' template.

  Example:
   (web-browser-ext-data [...])"
  [opt]
  (let [default nil]
    (merge default
           opt
           {:template :web-browser-ext
            :platform #{:javascript}})))


(defn- web-browser-ext-files
  "Returns paths for 'web-browser-ext' template.

  Example:
   (web-browser-ext-files {:name \"...\", ...})"
  [data]
  [["Makefile"                     (render "Makefile.web-ext" data)]
   ["project.clj"                  (render "project.web-ext.clj" data)]
   ["generator/externs_js.clj"     (render "externs_js.clj" data)]
   ["generator/manifest_json.clj"  (render "manifest_json.clj" data)]
   ["generator/option_html.clj"    (render "option_html.clj" data)]
   ["resources/background.js"      (render "background.js" data)]
   ["src/background/core.cljs"     (render "core.background.cljs" data)]
   ["src/content_script/core.cljs" (render "core.content-script.cljs" data)]
   ["src/option/core.cljs"         (render "core.option.cljs" data)]
   ["resources/clj.png"            (raw "clj.png")]
   ["resources/content-script.js"  (render "content-script.js" data)]
   ["resources/option.js"          (render "option.js" data)]
   ["resources/polyfill.js"        (render "polyfill.js" data)]
   ["tool/dev/repl.clj"            (render "tool-dev-repl.clj" data)]
   ["tool/dev/tool.cljs"           (render "tool-dev-tool.cljs" data)]
   ["tool/test/helper.cljs"        (render "test_helper.cljs" data)]
   ["tool/test/runner.cljs"        (render "test_runner.cljs" data)]
   ["tool/test/runner_macros.clj"  (render "test_runner_macros.clj" data)]])


(defn- web-server-data
  "Returns data for 'web-server' template.

  Example:
   (web-server-data {:db \"postgresql\"})"
  [opts]
  (let [default {:db "sqlite"}]
    (merge default
           opts
           {:template :web-server
            :flag #{:db-schema :server :web-ui}
            :platform #{:java :javascript}})))


(defn- web-server-files
  "Returns paths for 'web-server' template.

  Example:
   (web-server-files {:name \"...\", ...})"
  [data]
  (cond-> [[".env"                                          (render "_env" data)]
           ["Makefile"                                      (render "Makefile.web" data)]
           ["project.clj"                                   (render "project.web.clj" data)]
           ["tests.edn"                                     (render "tests.edn" data)]
           ,"data/migrations/"
           ["dev-resources/input.css"                       (render "tailwindcss.input.css" data)]
           ["gen/{{sanitized}}/core_gen.clj"                (render "core_gen.clj" data)]
           ,"resources/static/"
           ["resources/static/extra.js"                     (raw "extra.js")]
           ["spec/{{sanitized}}/core_spec.clj"              (render "core_spec.clj" data)]
           ["src/{{sanitized}}/core.clj"                    (render "core.server.clj" data)]
           ["src/{{sanitized}}/app/core.clj"                (render "app-core.web.clj" data)]
           ["src/{{sanitized}}/db/core.clj"                 (render "db-core.clj" data)]
           ["src/{{sanitized}}/db/var.clj"                  (render "db-var.clj" data)]
           ["src/{{sanitized}}/internal/db_util.clj"        (render "internal-db_util.clj" data)]
           ["src/{{sanitized}}/web_server/core.clj"         (render "web-core.clj" data)]
           ["src/{{sanitized}}/web_server/server.clj"       (render "web-server.clj" data)]
           ["src/{{sanitized}}/web_server/var.clj"          (render "web-var.clj" data)]
           ["tailwind.config.js"                            (render "tailwind.config.js" data)]
           ["test/{{sanitized}}/app/core_test.clj"          (render "app-core_test.web.clj" data)]
           ["test/{{sanitized}}/core_test.clj"              (render "core_test.clj" data)]
           ["test/{{sanitized}}/internal/db_util_test.clj"  (render "internal-db_util_test.clj" data)]
           ["test/{{sanitized}}/test_util.clj"              (render "test_util.clj" data)]
           ["test/spy/extension.clj"                        (render "spy-ext.clj" data)]
           ["tool/dev/tool.clj"                             (render "tool-dev-tool.clj" data)]
           ["tool/dev/tool.cljs"                            (render "tool-dev-tool-ws.cljs" data)]
           ["tool/dev/var.clj"                              (render "tool-dev-var.clj" data)]]
    (data :config) (append-conf-files data)) )


(defn- template-data
  "Returns template data.

  Example:
   (template-data \"my-proj-name\" \"cli\")"
  [name type opt]
  (let [git-config (some->> (sh "git" "config" "--global" "-l")
                            (:out)
                            (str/split-lines)
                            (map #(let [[k v] (str/split % #"=")]
                                    [(keyword k) v]))
                            (into {}))
        ver-data {:clj-toml-ver           "1.0.0-SNAPSHOT"
                  :clojure-ver            "1.11.1"
                  :clojurescript-ver      "1.11.60"
                  :core-async-ver         "1.6.673"
                  :criterium-ver          "0.4.6"
                  :data-json-ver          "2.4.0"
                  :data-xml-ver           "0.0.8"
                  :environ-ver            "1.2.0"
                  :figwheel-sidecar-ver   "0.5.20"
                  :garden-ver             "1.3.10"
                  :haslett-ver            "0.1.7"
                  :hiccup-ver             "2.0.0-RC1"
                  :honeysql-ver           "2.3.928"
                  :http-kit-ver           "2.7.0"
                  :lein-cljsbuild-ver     "1.1.8"
                  :lein-environ-ver       "1.2.0"
                  :migratus-lein-ver      "0.7.3"
                  :migratus-ver           "1.4.4"
                  :next-jdbc-ver          "1.3.834"
                  :piggieback-ver         "0.5.2"
                  :postgresql-ver         "42.5.0"
                  :react-dom-ver          "17.0.2-0"
                  :react-ver              "17.0.2-0"
                  :reagent-ver            "1.1.1"
                  :reitit-ver             "0.6.0"
                  :ring-core-ver          "1.9.6"
                  :ring-jetty-adapter-ver "1.9.6"
                  :ring-mock-ver          "0.4.0"
                  :slf4j-timbre-ver       "0.3.21"
                  :spy-ver                "2.13.0"
                  :sqlite-jdbc-ver        "3.39.2.1"
                  :test-check-ver         "1.1.1"
                  :tools-cli-ver          "1.0.214"
                  :timbre-ver             "6.1.0"
                  :tools-deps-alpha-ver   "0.14.1222"}
        user (System/getenv "USER")
        data {:name      name
              :ns-name   (lein/sanitize-ns name)
              :sanitized (lein/name-to-path name)
              :proj-name (str user "/" name)
              ;; addtional information
              :user      user
              :user-name (:user.name  git-config "FIXME")
              :email     (:user.email git-config "FIXME")
              :year      (lein/year)
              ;; default value for options
              :db        ""
              :config    false}
        type-data (condp = type
                    "api-server"      api-server-data
                    "cli"             cli-data
                    "web-browser-ext" web-browser-ext-data
                    "web-server"      web-server-data)]
    (merge data ver-data (type-data opt))))


(defn my-template
  "Generate Clojure Leiningen project.

  Available flags:
  - config

  Available types:
  - api-server
  - cli
  - electron
  - web-browser-ext
  - web-server

  Example:
   (my-template \"my-proj-name\" [\"+config\", \"-type\" \"api-server\", \"-db\" \"sqlite\"])"
  [name & args]
  (let [[flag-opts kw-opts] (split-with #(str/starts-with? % "+") args)
        opt (->> flag-opts
                 (map #(keyword (subs % 1)))
                 (map #(vector % true))
                 (into {}))
        opt (->> kw-opts
                 (partition 2)
                 (map (fn [[k v]]
                        (when (str/starts-with? k "-")
                          (-> k
                              (subs 1)
                              (keyword)
                              (vector v)))))
                 (remove nil?)
                 (into opt))
        type (let [type (:type opt "cli")
                   avail-types #{"api-server" "cli" "web-browser-ext" "web-server"}]
               (if (contains? avail-types type)
                 type
                 (do
                   (binding [*out* *err*]
                     (println (str "'" type "'") "is unknown type."))
                   (println "Here is available types:" (->> avail-types
                                                            (map #(str "'" % "'"))
                                                            (interpose ", ")
                                                            (apply str)))
                   (System/exit 1))))
        data (template-data name type opt)
        type-files (condp = type
                     "api-server"      api-server-files
                     "cli"             cli-files
                     "web-browser-ext" web-browser-ext-files
                     "web-server"      web-server-files)]
    (binding [*data* data]
      (apply lein/->files data
             (concat [[".gitignore"            (render "_gitignore" data)]
                      [".clj-kondo/config.edn" (render "clj-kondo-config.edn" data)]
                      ["README.md"             (render "README.md" data)]
                      ["LICENSE"               (render "LICENSE" data)]
                      "resources"]
                     (type-files data))))))


(comment
  (try
    (let [name "my-project"
          type "cli"
          opt {}
          data (template-data name type opt)]
      (binding [*data* data]
        (println (map first (cli-files data)))))
    (catch Exception e
      (println e))))
