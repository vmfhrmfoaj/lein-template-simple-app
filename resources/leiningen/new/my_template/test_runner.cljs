(ns ^:figwheel-always test.runner
  (:require-macros [test.runner-macros :refer [gen-ns-sym->test-fn]])
  (:require [cljs.test :as test]
            [test.helper]))


(gen-ns-sym->test-fn)


(defn run
  "Runs tests.

  Example:
   (run ...)"
  [env-or-ns & namespaces]
  (let [env (if (map? env-or-ns)
              env-or-ns
              (test/empty-env))
        namespaces  (cond->> namespaces (symbol? env-or-ns) (cons env-or-ns))
        summary     (atom {:test 0 :pass 0 :fail 0 :error 0 :type :summary})
        accumulator (fn []
                      (swap! summary
                             #(merge-with + %)
                             (:report-counters (test/get-and-clear-env!))))
        reporter (fn []
                   (test/set-env! env)
                   (test/do-report @summary)
                   (test/report (assoc @summary :type :end-run-tests))
                   (test/clear-env!))
        test-fns (select-keys ns-sym->test-fn namespaces)
        blocks   (concat (->> test-fns
                              (vals)
                              (map #(% env))
                              (mapcat #(concat % [accumulator])))
                         [reporter])]
    (if-not (empty? test-fns)
      (test/run-block blocks))))
