(ns <%= ns-name %>.test-util
  (:require [clojure.spec.alpha :as spec]
            [taoensso.timbre :as log]<%
          (when (#{:api-server :web-server} template) %>
            [<%= ns-name %>.db.core :as db]
            [<%= ns-name %>.internal.db-util :as db-util]
            [reitit.coercion.spec]
            [reitit.dev.pretty :as pretty]
            [reitit.ring :as ring]
            [reitit.ring.coercion :as mid.coercion]
            [reitit.ring.middleware.exception :as mid.exception]
            [reitit.ring.middleware.muuntaja :as mid.muuntaja]
            [reitit.ring.middleware.parameters :as mid.params]
            [muuntaja.core :as muuntaja]<% ) %>))


(defn attach-gen!
  "Attaches a generator to a spec.

  Example:
   (attatch-gen ::token #(gen/fmap str (gen/uuid)))"
  [spec gen-fn]
  (and (get (swap! @#'spec/registry-ref (fn [registry]
                                          (cond-> registry
                                            (get registry spec)
                                            (update spec #(spec/with-gen % gen-fn)))))
            spec)
       true))


(defmacro with-hard-redefs
  "Temporarily alters `clojure.lang.Var`s while executing the body that contains asynchronous code.
  At the end of body, you should wait until asynchronous code to complete.

  Example:
   (with-hard-redefs [+ #(apply + 10 %&)]
      (+ 1 2))"
  [bindings & body]
  (when-not (even? (count bindings))
    (throw (ex-info "'binding' should be in pairs" {:input [bindings body]})))

  (let [syms (->> bindings (take-nth 2) (map resolve))
        orig-vals (mapcat (juxt #(gensym (name (symbol %))) #(list 'var-get %)) syms)
        alter-var #(list 'alter-var-root %1 (list 'constantly %2))]
    `(let [~@orig-vals]
       (try
         ~@(->> bindings
                (drop 1)
                (take-nth 2)
                (map alter-var syms))
         ~@body
         (finally
           ~@(->> orig-vals (take-nth 2) (map alter-var syms)))))))


(defn without-trivial-log
  "Set log level to error to ignore debugging logs.

  Example:
   (clojure.test/use-fixtures :once without-trivial-log)"
  [func]
  (log/merge-config! {:min-level :error})
  (func))<%

(when (#{:api-server :web-server} template) %>


      (defn with-db
        "Initializes DB.

  Example:
   (clojure.test/use-fixtures :once with-db)"
        [func]
        (db/connect!)
        (func)
        (db/disconnect!))


      (def router-opt
        "Default route options."
        {:exception pretty/exception
         :data {:coercion reitit.coercion.spec/coercion
                :muuntaja muuntaja/instance
                :middleware [mid.params/parameters-middleware
                             mid.muuntaja/format-negotiate-middleware
                             mid.muuntaja/format-response-middleware
                             mid.exception/exception-middleware
                             mid.muuntaja/format-request-middleware
                             mid.coercion/coerce-response-middleware
                       mid.coercion/coerce-request-middleware]}})


(defn ring-handler
  "Returns a ring handler that created from given route data.

  Example:
   (clojure.test/use-fixtures :once ring-handler)"
  [route]
  (-> route (ring/router router-opt) (ring/ring-handler)))<% ) %>
