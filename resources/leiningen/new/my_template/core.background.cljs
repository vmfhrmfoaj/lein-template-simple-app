(ns <%= ns-name %>.background.core)


(defonce ^:private option
  (let [opt (atom nil)
        update! (fn [& _]
                  (.. js/browser
                      -storage
                      -local
                      (get #(reset! opt (js->clj % :keywordize-keys true)))))]
    (update!)
    (.. js/browser
        -storage
        -onChanged
        (addListener update!))
    opt))


(defn toolbar-click-event-handler
  [tab]
  (.. js/browser
      -tabs
      (sendMessage (.-id tab)
                   (clj->js @option))))


(defn ^:export index []
  (.. js/browser
      -browserAction
      -onClicked
      (addListener toolbar-click-event-handler)))
