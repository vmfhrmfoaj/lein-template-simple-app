(ns dev.tool
  (:require [clojure.data.xml :as xml]
            [clojure.java.io :as io]
            [clojure.tools.deps.alpha :as dep.tool]
            [clojure.tools.deps.alpha.util.maven :as dep.mvn]<%
          (when (#{:api-server :web-server} template) %>
            [<%= ns-name %>.app.core :as app]
            [<%= ns-name %>.core :as main]
            [<%= ns-name %>.web-server.core :as server]
            [org.httpkit.server :as http-kit]<% )
          (when (= :web-server template) %>
            [dev.var :as var]<% ) %>)
  (:import clojure.lang.DynamicClassLoader
           java.lang.management.ManagementFactory))


(defn- xml-tag-in
  [xml tag]
  (cond
    (map? xml)
    (if (= tag (:tag xml))
      xml (xml-tag-in (:content xml) tag))

    (sequential? xml)
    (->> xml
         (filter coll?)
         (reduce #(when-let [xml (xml-tag-in %2 tag)]
                    (reduced xml))
                 nil))))


(defn- repo []
  (let [mvn-conf-file (io/file (System/getProperty "user.home") ".m2" "settings.xml")
        custom-repos (when (.exists mvn-conf-file)
                       (let [conf (some-> mvn-conf-file (io/reader) (xml/parse))
                             mirrors (some-> conf
                                             (xml-tag-in :settings)
                                             (xml-tag-in :mirrors)
                                             :content)]
                         (some->> mirrors
                                  (filter coll?)
                                  (map (juxt #(some-> % (xml-tag-in :mirrorOf) :content (first))
                                             #(some-> % (xml-tag-in :url)      :content (first))))
                                  (into {}))))]
    (merge dep.mvn/standard-repos custom-repos)))


(defn- dyn-class-loader []
  (->> (.getContextClassLoader (Thread/currentThread))
       (iterate #(.getParent %))
       (take-while #(instance? DynamicClassLoader %))
       (last)))


(defn add-lib
  "Adds a library dynamically.

  Example:
   (add-lib [\"org.clojure/core.async\" \" 1.6.673\"])"
  [[lib ver] & exclusions]
  ;; NOTE
  ;;  Reference implementation `add-libs` in https://github.com/clojure/tools.deps.alpha/blob/add-lib3/src/main/clojure/clojure/tools/deps/alpha/repl.clj
  (let [loader (dyn-class-loader)
        sym (symbol (cond-> lib
                      (not (namespace (symbol lib)))
                      (str "/" lib)))
        deps {sym {:mvn/version ver :exclusions (conj exclusions 'org.clojure/clojure)}}
        lib (dep.tool/resolve-deps {:deps deps :mvn/repos (repo)} nil)
        paths (->> lib
                   (vals)
                   (map :paths)
                   (flatten))]
    (->> paths
         (map io/file)
         (map #(.toURL %))
         (run! #(.addURL loader %)))
    paths))<%


(when (= :web-server template) %>


(defn ws-handler
  "Returns a Ring handler for WebSocket.

  Example:
   (ws-handler {...})"
  [_context]
  {:get {:handler (fn [req]
                    (if-not (:websocket? req)
                      {:status 400, :body {:error "no websocket request"}}
                      (http-kit/as-channel req
                                           {:on-close (fn [ch status]
                                                        (println "web-socket closed:" {:channel ch :status status})
                                                        (reset! var/ws-ch nil))

                                            :on-open  (fn [ch]
                                                        (println "web-socket open:" {:channel ch})
                                                        (reset! var/ws-ch ch))})))}})


(defn reload-web-browser
  "Send a notification to the web browser to reload the current page." []
  (when-let [ch @var/ws-ch]
    (http-kit/send! ch (str {:cmd :reload}))))<% ) %>


(defn on-load
  "This function will be called automatically after starting REPL." []
  ;; NOTE
  ;;  If you use Emacs + Cider, you need to add below code snippet to your Emacs config to automatically start servers.
  ;;   ```emacs-lisp
  ;;   (add-hook 'cider-connected-hook
  ;;            (lambda ()
  ;;                    (cider-ns-refresh)
  ;;                    (cider-nrepl-request:eval
  ;;                     (concat "(try"
  ;;                             "  (require 'dev.tool)"
  ;;                             "  (some-> 'dev.tool/on-load (find-var) (.invoke))"
  ;;                             "  (catch Exception _))")
  ;;                     (cider-eval-print-handler)
  ;;                     "dev.tool" nil nil nil
  ;;                     (cider-current-connection))))
  ;;   ````
  <% (when (= :web-server template)
%>(println "start ws-server!")
  (let [ctx (main/context *command-line-args*)]
    (when-not @var/ws-server
      (let [ctx (assoc-in ctx [:server :port] 3030)]
        (reset! var/ws-server (server/start ctx [["/ws" (ws-handler ctx)]])))))<% ) %>)


(defn on-reload
  "This function will be called automatically after reloading the namespace." []
  ;; NOTE
  ;;  If you use Emacs + Cider, you need to add following code to your Eamcs config to automatically reload namespaces when saving a file.
  ;;   ```emacs-lisp
  ;;   (add-hook 'clojure-mode-hook (lambda () (add-hook 'after-save-hook (lambda () (cider-ns-refresh)))))
  ;;   (advice-add #'cider-ns-refresh--handle-response :before
  ;;               (lambda (response log-buffer)
  ;;                 "Do some actions for example catching error messages from the refresh log"
  ;;                 (nrepl-dbind-response response (status error)
  ;;                   (cond
  ;;                     ...
  ;;                     ((member "ok" status)
  ;;                      (cider-map-repls :clj
  ;;                        (lambda (conn)
  ;;                                (cider-nrepl-request:eval
  ;;                                 (concat "(try"
  ;;                                         "  (require 'dev.tool)"
  ;;                                         "  (some-> 'dev.tool/on-reload (find-var) (.invoke))"
  ;;                                         "  (catch Exception _))")
  ;;                                 (cider-eval-print-handler)
  ;;                                 "dev.tool" nil nil nil conn))))
  ;;   ```
  <% (when (#{:api-server :web-server} template)
%>(when (server/running?)
    (println "restart server!")
    (server/stop!)
    (let [ctx (main/context *command-line-args*)]
      (server/start! ctx (app/routes ctx)))<%
  (when (= :web-server template) %>
    (reload-web-browser)<% ) %>)<% ) %>)


(defn get-jvm-args
  "Returns JVM arguments." []
  (.getInputArguments (ManagementFactory/getRuntimeMXBean)))


(comment
  (add-lib ['org.clojure/core.async "1.6.673"]))

(comment
  (when-let [server @var/ws-server]
    (server/stop server))
  (let [ctx {:server {:port 3030}}]
    (reset! var/ws-server (server/start ctx [["/ws" (ws-handler ctx)]]))))
