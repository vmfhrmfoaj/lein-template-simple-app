(ns <%= ns-name %>.core-test
  (:require [clojure.test :refer :all]
            [clojure.spec.alpha :as spec]
            [clojure.spec.gen.alpha :as gen]
            [<%= ns-name %>.core :as target]
            [<%= ns-name %>.core-spec :as target-spec]
            [<%= ns-name %>.core-gen]
            [<%= ns-name %>.test-util :as util]
            [spy.core :as spy]))

(use-fixtures :once util/without-trivial-log<% (when (#{:api-server :web-server} template) %> util/with-db<% ) %>)


(deftest example
  (testing "how to use `spy`"
    (is (let [x (spy/stub nil)]
          (x 1 2 3 4)
          (spy/called-with? x 1 2 3 4))))

  (testing "how to use `clojure.spec.gen`"
    (is (get (->> "simple"
                  (map str)
                  (into #{}))
             (gen/generate (spec/gen ::target-spec/simple))))))
