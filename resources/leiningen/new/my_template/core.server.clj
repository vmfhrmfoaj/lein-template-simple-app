(ns <%= ns-name %>.core
  (:gen-class)
  (:require [<%= ns-name %>.app.core :as app]
            [<%= ns-name %>.db.core :as db]
            [<%= ns-name %>.web-server.core :as server]
            [taoensso.timbre :as log])
  (:import java.util.TimeZone))

(set! *warn-on-reflection* true)


(defn context
  "Returns a context map.

  Examples:
   (context)
   (context *command-line-args*)"
  ([]
   (context nil))
  ([_args]
   nil))


(defn -main
  [& args]
  (log/merge-config! {:min-level :warn, :timestamp-opts {:timezone (TimeZone/getDefault)}})
  (db/connect!)
  (let [ctx (context args)]
    (server/start! ctx (app/routes ctx))))


(comment
  (db/connect!)
  (alter-var-root #'clojure.core/*command-line-args* (constantly nil))
  (let [ctx (context *command-line-args*)]
    (server/start! ctx (app/routes ctx))))
