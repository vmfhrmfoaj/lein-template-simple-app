(ns ^{:clojure.tools.namespace.repl/load   false
      :clojure.tools.namespace.repl/unload false}
  <%= ns-name %>.web-server.var)

(set! *warn-on-reflection* true)


(def server
  "Server instance."
  (atom nil))
