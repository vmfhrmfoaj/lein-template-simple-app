(ns <%= ns-name %>.config-spec
  (:require [clojure.spec.alpha :as spec]
            [clojure.spec.alpha.extension :refer [with-explain]]))

(set! *warn-on-reflection* true)


(spec/def ::config
  (with-explain
    map?
    "It should be table"))
