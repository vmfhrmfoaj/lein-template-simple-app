LEIN ?= lein

GROUP_NAME := $(shell cat project.clj | tr -d '\n' | awk 'match($$0,/defproject\s+([^ \/]+)\/([^ ]+)\s+"([^"]+)"/,m)  { print m[1] }')
PROJ_NAME  := $(shell cat project.clj | tr -d '\n' | awk 'match($$0,/defproject\s+([^ \/]+\/)?([^ ]+)\s+"([^"]+)"/,m) { print m[2] }')
PROJ_VER   := $(shell cat project.clj | tr -d '\n' | awk 'match($$0,/defproject\s+([^ \/]+\/)?([^ ]+)\s+"([^"]+)"/,m) { print m[3] }')

SRC_FILES      := $(shell find . -name '*.clj' -or -name '*.clj[cs]' 2>/dev/null)
RESOURCE_FILES := $(shell find resources -name '*' 2>/dev/null)
JAR_FILE     := target/$(PROJ_NAME)-$(PROJ_VER).jar
UBERJAR_FILE := target/$(PROJ_NAME)-$(PROJ_VER)-standalone.jar
INSTALLED_JAR_FILE := $(HOME)/.m2/repository/$(GROUP_NAME)/$(PROJ_NAME)/$(PROJ_VER)/$(PROJ_NAME)-$(PROJ_VER).jar

.PHONY: build
build: $(JAR_FILE)

.PHONY: test
test:
	$(LEIN) trampoline test

.PHONY: isntall
install: $(INSTALLED_JAR_FILE)

.PHONY: jar
jar: $(JAR_FILE)

.PHONY: uberjar
uberjar: $(UBERJAR_FILE)

.PHONY: deploy
deploy: clean $(UBERJAR_FILE)
	$(LEIN) deploy

.PHONY: clean
clean:
	$(LEIN) clean

pom.xml: project.clj
	$(LEIN) pom

$(JAR_FILE): project.clj $(SRC_FILES) $(RESOURCE_FILES)
ifneq (install,$(MAKECMDGOALS))
	$(LEIN) jar  # `$(LEIN) install` will generate a jar file, so don't need generate it here.
endif
	@touch $@

$(UBERJAR_FILE): project.clj $(SRC_FILES) $(RESOURCE_FILES)
	$(LEIN) uberjar
	@touch $@

$(INSTALLED_JAR_FILE): $(JAR_FILE)
	$(LEIN) install
	@touch $@
