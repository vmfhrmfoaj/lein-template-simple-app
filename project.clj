(defproject vmfhrmfoaj/my-lein-templates
  "1.1.0-SNAPSHOT"

  :description "My Leiningen template collections."
  :url "https://gitlab.com/vmfhrmfoaj/my-lein-templates"
  :license {:name "The MIT License"
            :url "http://opensource.org/licenses/MIT"}

  :dependencies [[comb "0.1.1"]]

  :profiles
  {:dev {:repl-options {:init-ns user}}}

  :eval-in-leiningen true)
